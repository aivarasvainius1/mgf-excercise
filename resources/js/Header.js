import React from 'react';
import { Link } from 'react-router-dom';
import {
    Navbar,
    Nav,
    NavItem
} from 'reactstrap';
const Header = () => {
    return (
        <div>
            <Navbar color="light" light expand="md">
                <Nav className="mr-auto" navbar>
                    <NavItem>
                        <Link to="/designs" className="nav-link">Designs</Link>
                    </NavItem>
                </Nav>
            </Navbar>
        </div>
    );
};

export default Header;