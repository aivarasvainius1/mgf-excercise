import * as actionTypes from '../actions/actionTypes';

const initialState = {
    loading: true,
    errors: [],
    data: [],
    item: [],
    issueCreated: false
};

const designs = (state = initialState, action ) => {
    switch (action.type) {
        case actionTypes.DESIGNS_FETCH_START:
            return {
                ...state,
                loading: true
            };
        case actionTypes.DESIGNS_FETCH_SUCCESS:
            return {
                ...state,
                loading: false,
                data: action.data,
            };
        case actionTypes.DESIGNS_FETCH_ERROR:
            return {
                ...state,
                loading: false,
                errors: action.error
            };
        case actionTypes.DESIGN_FETCH_START:
            return {
                ...state,
                loading: true
            };
        case actionTypes.DESIGN_FETCH_SUCCESS:
            return {
                ...state,
                loading: false,
                item: action.data,
            };
        case actionTypes.DESIGN_FETCH_ERROR:
            return {
                ...state,
                loading: false,
                errors: action.error
            };
        case actionTypes.DESIGN_ISSUE_CREATE_SUCCESS:
            return {
                ...state,
                issueCreated: true
            };
        case actionTypes.RESET_ISSUE_CREATED:
            return {
                ...state,
                issueCreated: false
            }
        default:
            return state;
    }
};

export default designs;