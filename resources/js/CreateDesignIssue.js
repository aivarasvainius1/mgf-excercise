import React, { Component } from 'react';
import { Row, Col, Form, FormGroup, Label, Input, Button } from 'reactstrap';
import { fetchDesign, createDesignIssue, resetIssueCreated } from "./actions/designs";
import { connect } from "react-redux";
import { Link } from 'react-router-dom';

class CreateDesignIssue extends Component {

    constructor(props) {
        super(props);
        this.state = {
            description: '',
            status: 'test',
        };
    }

    componentDidMount() {
        this.props.fetchDesign(this.props.match.params.id)
    }

    componentDidUpdate(prevProps) {
        if (this.props.created) {
            this.props.history.push('/designs')
        }
        this.props.resetIssueCreated();
    }

    handleDescriptionChange(event) {
        this.setState({description: event.target.value});
    }

    handleSubmit(event) {
        event.preventDefault();
        this.props.createDesignIssue(this.props.match.params.id, {description: this.state.description, status: this.state.status});
    }

    render() {
        if (this.props.loading) {
            return null;
        } else {
            return (
                <div>
                    <h4 className="mb-4">Create issue for {this.props.item.title}</h4>
                    <hr/>
                    <Row>
                        <Col sm="7">
                            <Form onSubmit={(event) => this.handleSubmit(event)}>
                                <FormGroup>
                                    <Label for="exampleText">Description</Label>
                                    <Input value={this.state.description} onChange={(event) => this.handleDescriptionChange(event)} type="textarea" name="text" id="exampleText" />
                                </FormGroup>
                                <Button color="success">Submit</Button>
                                <Link to="/designs" className="btn btn-danger ml-2">Cancel</Link>
                            </Form>
                        </Col>
                    </Row>
                </div>
            );
        }
    }
}

const mapStateToProps = state => {
    return {
        item: state.designs.item,
        loading: state.designs.loading,
        created: state.designs.issueCreated
    }
};

const mapDispatchToProps = dispatch => {
    return {
        fetchDesign: (id) => dispatch(fetchDesign(id)),
        createDesignIssue: (designId, designIssue) => dispatch(createDesignIssue(designId, designIssue)),
        resetIssueCreated: () => dispatch(resetIssueCreated())
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(CreateDesignIssue);