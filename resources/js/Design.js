import React, { Component } from 'react';
import { Row, Col } from 'reactstrap';
import { Link } from 'react-router-dom';
import {fetchDesign} from "./actions/designs";
import {connect} from "react-redux";

class Design extends Component {

    componentDidMount() {
        console.log('dd');
        this.props.fetchDesign(this.props.match.params.id)
    }

    render() {
        if (this.props.loading) {
            return null;
        } else {
            console.log(this.props.item.issues);
            return (
                <div>
                    <Row>
                        <Col sm="11"><h4 className="mb-2">{this.props.item.title}</h4></Col>
                        <Col><Link to="/designs">Back</Link></Col>
                    </Row>
                    <hr/>
                    <ul>
                        <li>Special project: <b>{this.props.item.special_project ? 'Yes' : 'No'}</b></li>
                        <li>Permanent works: <b>{this.props.item.permanent_works ? 'Yes' : 'No'}</b></li>
                        <li>Depth: <b>{this.props.item.depth}</b></li>
                        <li>Length: <b>{this.props.item.length}</b></li>
                        <li>Width: <b>{this.props.item.width}</b></li>
                    </ul>
                    <h5>Issues:</h5>
                    <ul>
                        {
                            this.props.item.issues.map((issue) => {
                                return <li key={issue.id}>{issue.description}</li>;
                            })
                        }
                    </ul>
                </div>
            );
        }
    }
};

const mapStateToProps = state => {
    return {
        item: state.designs.item,
        loading: state.designs.loading
    }
};

const mapDispatchToProps = dispatch => {
    return {
        fetchDesign: (id) => dispatch(fetchDesign(id)),

    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Design);
