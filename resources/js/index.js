import React  from 'react';
import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import Main from './Main';
import designs from './reducers/designs'
import axios from 'axios';

const rootReducer = combineReducers({
    designs: designs
});

axios.defaults.baseURL = 'http://nutri.test/api/';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(rootReducer, composeEnhancers(applyMiddleware(thunk)));



if (document.getElementById('app')) {
    ReactDOM.render(<Provider store={store}><Main /></Provider>, document.getElementById('app'));
}
