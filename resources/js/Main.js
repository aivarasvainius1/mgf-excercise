import React from 'react';
import Designs from './Designs';
import Design from './Design';
import Header from './Header';
import { BrowserRouter, Route } from 'react-router-dom';
import { Container } from 'reactstrap';
import CreateDesignIssue from "./CreateDesignIssue";

const Main = () => {
    return(
        <BrowserRouter>
            <div>
                <Header />
                <Container className="mt-4">
                    <Route path="/designs" component={Designs} exact/>
                    <Route path="/designs/:id" component={Design} exact/>
                    <Route path="/designs/:id/issues/create" component={CreateDesignIssue} exact/>
                </Container>
            </div>
        </BrowserRouter>
    );
};

export default Main;