import axios from "axios";
import * as actionTypes from './actionTypes';

export const fetchDesignsStart = () => {
    return {
        type: actionTypes.DESIGNS_FETCH_START
    }
};

export const fetchDesignsSuccess = (data) => {
    return {
        type: actionTypes.DESIGNS_FETCH_SUCCESS,
        data: data
    }
};

export const fetchDesignsError = (error) => {
    return {
        type: actionTypes.DESIGNS_FETCH_ERROR,
        error: error
    }
};

export const fetchDesigns = () => {
    return ( dispatch ) => {
        dispatch(fetchDesignsStart());
        axios.get('designs')
            .then(response => {
                dispatch(fetchDesignsSuccess(response.data.data));
            })
            .catch(err => {
                console.log('failed');
            })
    }
};

export const fetchDesignStart = () => {
    return {
        type: actionTypes.DESIGN_FETCH_START
    }
};

export const fetchDesignSuccess = (data) => {
    return {
        type: actionTypes.DESIGN_FETCH_SUCCESS,
        data: data
    }
};

export const fetchDesignError = (error) => {
    return {
        type: actionTypes.DESIGN_FETCH_ERROR,
        error: error
    }
};

export const fetchDesign = (id) => {
    return ( dispatch ) => {
        dispatch(fetchDesignStart(id));
        axios.get('designs/'+id)
            .then(response => {
                dispatch(fetchDesignSuccess(response.data.data));
            })
            .catch(err => {
                console.log('failed');
            })
    }
};

export const createDesignIssueStart = () => {
    return {
        type: actionTypes.DESIGN_ISSUE_CREATE_START
    }
};

export const createDesignIssueSuccess = (data) => {
    return {
        type: actionTypes.DESIGN_ISSUE_CREATE_SUCCESS,
        data: data,
        issueCreated: true
    }
};

export const createDesignIssueError = (error) => {
    return {
        type: actionTypes.DESIGN_ISSUE_CREATE_ERROR,
        error: error
    }
};

export const resetIssueCreated = () => {
    return {
        type: actionTypes.RESET_ISSUE_CREATED
    }
};

export const createDesignIssue = (designId, designIssue) => {
    return ( dispatch ) => {
        dispatch(createDesignIssueStart());
        axios.post('designs/' + designId + '/issues', {...designIssue})
            .then(response => {
                dispatch(createDesignIssueSuccess(response.data.data));
            })
            .catch(err => {
                console.log('failed');
            })
    }
};
