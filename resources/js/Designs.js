import React, {Component} from 'react';
import { Table, Button } from 'reactstrap';
import { Link } from 'react-router-dom';
import { fetchDesigns } from "./actions/designs";
import { connect } from 'react-redux';

class Designs extends Component {

    componentDidMount() {
        this.props.fetchDesigns();
    }

    render() {
        if (this.props.loading) {
            return null;
        } else {
            return (
                <div>
                    <h2 className="mb-4">Designs</h2>

                    <Table className="table-bordered">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Title</th>
                            <th>Special project?</th>
                            <th>Permanent works?</th>
                            <th>Depth</th>
                            <th>Length</th>
                            <th>Width</th>
                            <th>Issues</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        {
                            this.props.data.map((design) => {
                                return (
                                    <tr key={design.id}>
                                        <th scope="row">{design.id}</th>
                                        <td>{design.title}</td>
                                        <td>{design.special_project ? 'Yes' : 'No'}</td>
                                        <td>{design.permanent_works ? 'Yes' : 'No'}</td>
                                        <td>{design.depth}</td>
                                        <td>{design.length}</td>
                                        <td>{design.width}</td>
                                        <td>{design.issues.length}</td>
                                        <td>
                                            <Link to={'/designs/' + design.id}>
                                                <Button color="info" size="sm" className="mr-2">View</Button>
                                            </Link>
                                            <Link to={'designs/'+ design.id +'/issues/create'}>
                                                <Button color="primary" size="sm">Create issue</Button>
                                            </Link>
                                        </td>
                                    </tr>
                                );
                            })
                        }
                        </tbody>
                    </Table>
                </div>
            );
        }
    }
};

const mapStateToProps = state => {
    return {
        data: state.designs.data,
        loading: state.designs.loading
    }
};

const mapDispatchToProps = dispatch => {
    return {
        fetchDesigns: () => dispatch(fetchDesigns()),

    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Designs);