<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'UserController@authenticate');
Route::apiResource('designs', 'DesignController');
Route::get('user', 'UserController@getAuthenticatedUser');
Route::post('designs/{design}/issues', 'DesignIssueController@store');

Route::group(['middleware' => ['jwt.verify']], function() {

});

