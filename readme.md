1. Clone repository
2. Install dependencies managed by composer | composer install
3. Run migrations | php artisan migrate
4. Run seeds | php artisan db:seed
5. Install front end dependencies | npm install
6. Compile assets | npm run dev
7. Change link in resources/js/index.js, line 15