<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DesignIssue extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'design_id',
        'description',
        'dateIn',
        'dateOut',
        'designer_id',
        'checker_id',
        'status',
        'drawing_req',
    ];

    /**
     * Get the design that owns the issue.
     */
    public function design()
    {
        return $this->belongsTo(Design::class);
    }
}
