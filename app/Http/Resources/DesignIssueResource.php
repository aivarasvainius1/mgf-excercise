<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class DesignIssueResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'description' => $this->description,
            'dateIn' => $this->dateIn,
            'dateOut' => $this->dateOut,
            'status' => $this->status,
            'drawing_req' => $this->drawing_req,
            'design' => $this->design
        ];
    }
}
