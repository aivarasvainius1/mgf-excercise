<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class DesignResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'special_project' => $this->special_project,
            'permanent_works' => $this->permanent_works,
            'depth' => $this->depth,
            'length' => $this->length,
            'width' => $this->width,
            'created_at' => (string) $this->created_at,
            'updated_at' => (string) $this->updated_at,
            'issues' => $this->issues
        ];
    }
}
