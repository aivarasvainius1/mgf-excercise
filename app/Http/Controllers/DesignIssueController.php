<?php

namespace App\Http\Controllers;

use App\Design;
use App\DesignIssue;
use App\Http\Resources\DesignIssueResource;
use Illuminate\Http\Request;

class DesignIssueController extends Controller
{
    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request, Design $design)
    {
        $issue = DesignIssue::create(
            [
                'designer_id' => 1,
                'checker_id' => 2,
                'design_id' => $design->id,
                'description' => $request->description,
                'dateIn' => new \DateTime(),
                'dateOut' => new \DateTime(),
                'status' => $request->status,
                'drawing_req' => true
            ]
        );

        return new DesignIssueResource($issue);
    }
}
