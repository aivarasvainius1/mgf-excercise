<?php

namespace App\Http\Controllers;

use App\Design;
use App\Http\Resources\DesignResource;
use Illuminate\Http\Request;

class DesignController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return DesignResource::collection(Design::all());
    }
}
