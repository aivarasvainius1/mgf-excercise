<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Design extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'special_project',
        'permanent_works',
        'depth',
        'length',
        'width',
    ];

    /**
     * Get issues for design.
     */
    public function issues()
    {
        return $this->hasMany(DesignIssue::class);
    }
}
