<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDesignIssuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('design_issues', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('design_id');
            $table->foreign('design_id')->references('id')->on('designs');
            $table->text('description');
            $table->dateTime('dateIn');
            $table->dateTime('dateOut');
            $table->unsignedInteger('designer_id');
            $table->foreign('designer_id')->references('id')->on('users');
            $table->unsignedInteger('checker_id');
            $table->foreign('checker_id')->references('id')->on('users');
            $table->string('status');
            $table->boolean('drawing_req');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('design_issues');
    }
}
