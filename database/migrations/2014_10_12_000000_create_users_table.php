<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('email')->unique();
            $table->string('initial')->nullable();
            $table->string('grade')->nullable();
            $table->string('title')->nullable();
            $table->string('qualifications')->nullable();
            $table->string('password');
            $table->text('eductaion')->nullable();
            $table->text('synopsis')->nullable();
            $table->text('memberships')->nullable();
            $table->text('experience')->nullable();
            $table->boolean('senior')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
