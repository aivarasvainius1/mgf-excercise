<?php

use Illuminate\Database\Seeder;

class DesignsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 0; $i < 10; $i++)
        DB::table('designs')->insert([
            'title' => 'Design #'.($i + 1),
            'special_project' => rand(0, 1),
            'permanent_works' => rand(0, 1),
            'depth' => rand(0, 100),
            'length' => rand(0, 100),
            'width' => rand(0, 100),
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
    }
}
