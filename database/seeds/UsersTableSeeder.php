<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Jane McDonald (senior)',
            'email' => 'janemcdonald@gmail.com',
            'initial' => 'S',
            'grade' => 'r',
            'title' => 'Mrs',
            'qualifications' => null,
            'eductaion' => '1st Class BSc Civil Engineering',
            'synopsis' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In in magna purus. Donec interdum quis est in ultrices. Integer nec ex vitae neque consequat aliquam sed nec neque. In sem purus, egestas at interdum convallis, egestas ut erat. Morbi posuere justo id erat porta consequat. Praesent purus est, eleifend sit amet libero ut, congue venenatis turpis. Curabitur tristique, lorem eget ornare dignissim, lorem sapien bibendum enim, eget fermentum quam ex vel metus. Nam condimentum tellus nec nibh egestas, eu finibus nisl gravida.',
            'memberships' => 'Chartered Member Institute of Civil Engineers',
            'experience' => 'Vivamus efficitur urna nulla, porttitor venenatis leo scelerisque vitae. Cras non turpis magna. Vestibulum laoreet neque lobortis ligula tristique pulvinar. Ut molestie gravida rutrum. Integer sagittis mi mauris, vel interdum ex suscipit vel. Mauris eleifend scelerisque mauris mattis mollis. Mauris urna ante, fringilla eu convallis eget, congue vel tortor. Morbi varius dapibus purus, at imperdiet orci viverra nec. Nulla sit amet vulputate augue. Sed efficitur tincidunt dolor, eu porttitor eros dapibus a. Sed dignissim condimentum ipsum pellentesque maximus. Fusce porta placerat pellentesque. Proin finibus a ipsum vel accumsan. Nulla et eleifend metus, porta tempus magna.',
            'senior' => true,
            'password' => bcrypt('secret'),
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);

        DB::table('users')->insert([
            'name' => 'Bart Simpson (senior)',
            'email' => 'bartsimpson@gmail.com',
            'initial' => 'H',
            'grade' => 's',
            'title' => 'Mr',
            'qualifications' => 'BSc (Hons)',
            'eductaion' => null,
            'synopsis' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In in magna purus. Donec interdum quis est in ultrices. Integer nec ex vitae neque consequat aliquam sed nec neque. In sem purus, egestas at interdum convallis, egestas ut erat. Morbi posuere justo id erat porta consequat. Praesent purus est, eleifend sit amet libero ut, congue venenatis turpis. Curabitur tristique, lorem eget ornare dignissim, lorem sapien bibendum enim, eget fermentum quam ex vel metus. Nam condimentum tellus nec nibh egestas, eu finibus nisl gravida.',
            'memberships' => 'Chartered Member Institute of Civil Engineers',
            'experience' => 'Vivamus efficitur urna nulla, porttitor venenatis leo scelerisque vitae. Cras non turpis magna. Vestibulum laoreet neque lobortis ligula tristique pulvinar. Ut molestie gravida rutrum. Integer sagittis mi mauris, vel interdum ex suscipit vel. Mauris eleifend scelerisque mauris mattis mollis. Mauris urna ante, fringilla eu convallis eget, congue vel tortor. Morbi varius dapibus purus, at imperdiet orci viverra nec. Nulla sit amet vulputate augue. Sed efficitur tincidunt dolor, eu porttitor eros dapibus a. Sed dignissim condimentum ipsum pellentesque maximus. Fusce porta placerat pellentesque. Proin finibus a ipsum vel accumsan. Nulla et eleifend metus, porta tempus magna.',
            'senior' => true,
            'password' => bcrypt('secret'),
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);

        DB::table('users')->insert([
            'name' => 'Brian Griffin',
            'email' => 'briangriffin@gmail.com',
            'initial' => null,
            'grade' => 'a',
            'title' => 'Mr',
            'qualifications' => 'mEng(2:ii)',
            'eductaion' => null,
            'synopsis' => null,
            'memberships' => null,
            'experience' => null,
            'senior' => false,
            'password' => bcrypt('secret'),
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);

        DB::table('users')->insert([
            'name' => 'Jim Bowen',
            'email' => 'jimbowen@gmail.com',
            'initial' => null,
            'grade' => 'b',
            'title' => 'Mr',
            'qualifications' => 'bTec',
            'eductaion' => null,
            'synopsis' => null,
            'memberships' => null,
            'experience' => null,
            'senior' => false,
            'password' => bcrypt('secret'),
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
    }
}
