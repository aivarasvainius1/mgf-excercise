<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class DesignIssuesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 0; $i < 15; $i++) {
            DB::table('design_issues')->insert([
                'design_id' => rand(1, 10),
                'description' => 'Issue #'.($i+1),
                'dateIn' => Carbon::now(),
                'dateOut' => Carbon::now(),
                'designer_id' => 1,
                'checker_id' => 2,
                'status' => 'temporary',
                'drawing_req' => rand(0, 1),
                'created_at' => new DateTime(),
                'updated_at' => new DateTime()
            ]);
        }
    }
}
